class PoniesController < ApplicationController
  before_action :set_pony, only: [:show, :edit, :update, :destroy]

  # GET /ponies
  def index
    @ponies = Pony.all
  end

  # GET /ponies/1
  def show
  end

  # GET /ponies/new
  def new
    @pony = Pony.new
  end

  # GET /ponies/1/edit
  def edit
  end

  # POST /ponies
  def create
    @pony = Pony.new(pony_params)

    if @pony.save
      redirect_to @pony, notice: 'Pony was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /ponies/1
  def update
    if @pony.update(pony_params)
      redirect_to @pony, notice: 'Pony was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /ponies/1
  def destroy
    @pony.destroy
    redirect_to ponies_url, notice: 'Pony was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pony
      @pony = Pony.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def pony_params
      params.require(:pony).permit(:name, :description, :color)
    end
end
