Rails.application.routes.draw do
  root to: 'ponies#index'

  resources :ponies
end
